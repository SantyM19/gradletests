package util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StringUtilTest {
    @Test
    public void testRepeatZero(){
        String result = StringUtil.repeat("hi",0);
        assertEquals("",result);;
    }

    @Test
    public void testRepeatOnce(){
        String result = StringUtil.repeat("hi",1);
        assertEquals("hi",result);;
    }

    @Test
    public void testRepeatMultipleTimes(){
        String result = StringUtil.repeat("hi",3);
        assertEquals("hihihi",result);
    }

    //IsEmpty

    @Test
    public void not_empty_string(){
        assertFalse(StringUtil.isEmpty("any string"));
    }

    @Test
    public void null_empty_string(){
        assertTrue(StringUtil.isEmpty(null));
    }

    @Test
    public void empty_string(){
        assertTrue(StringUtil.isEmpty(""));
    }

    @Test
    public void empty_only_spaces_string(){
        assertTrue(StringUtil.isEmpty("   "));
    }
}