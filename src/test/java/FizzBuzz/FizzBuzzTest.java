package FizzBuzz;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FizzBuzzTest {
    /*
        Si el número es divisible por 3, retorna Fizz
        Si el número es divisible por 5, retorna Buzz
        Si el número es divisible por 3 y por 5, retorna FizzBuzz
        En otro caso, retorna el mismo número
        fizzBuzz(3) ⇒ "Fizz"
        fizzBuzz(6) ⇒ "Fizz"
        fizzBuzz(5) ⇒ "Buzz"
        fizzBuzz(10) ⇒ "Buzz"
        fizzBuzz(15) ⇒ "FizzBuzz"
        fizzBuzz(30) ⇒ "FizzBuzz"
        fizzBuzz(2) ⇒ "2"
        fizzBuzz(16) ⇒ "16"
        */
    @Test
    public void return_fizz_when_number_is_divisible_by_3() {
        assertEquals( FizzBuzz.fizzBuzz(3),"Fizz" );
        assertEquals( FizzBuzz.fizzBuzz(6),"Fizz" );
    }

    @Test
    public void return_buzz_when_number_is_divisible_by_5() {
        assertEquals( FizzBuzz.fizzBuzz(5), "Buzz" );
        assertEquals( FizzBuzz.fizzBuzz(10), "Buzz" );
    }

    @Test
    public void return_fizzbuzz_when_number_is_divisible_by_3_and_5() {
        assertEquals( FizzBuzz.fizzBuzz(15), "FizzBuzz" );
        assertEquals( FizzBuzz.fizzBuzz(30), "FizzBuzz" );
    }

    @Test
    public void return_number_when_number_is_not_divisible_by_3_or_5() {
        assertEquals( FizzBuzz.fizzBuzz(2), "2" );
        assertEquals( FizzBuzz.fizzBuzz(16), "16" );
    }

}