package romanNumerals;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RomanNumeralShould {
    @Test
    public void number_is_1_then_I() {
        assertEquals(RomanNumeral.arabicToRoman(1), "I");
    }

    @Test
    public void number_is_2_then_II() {
        assertEquals(RomanNumeral.arabicToRoman(2), "II");
    }

    @Test
    public void number_is_3_then_III() {
        assertEquals(RomanNumeral.arabicToRoman(3), "III");
    }

    @Test
    public void number_is_5_then_V() {
        assertEquals(RomanNumeral.arabicToRoman(5), "V");
    }

    @Test
    public void number_is_6_then_VI() {
        assertEquals(RomanNumeral.arabicToRoman(6), "VI");
    }

    @Test
    public void number_is_7_then_VII() {
        assertEquals(RomanNumeral.arabicToRoman(7), "VII");
    }

    @Test
    public void number_is_10_then_X() {
        assertEquals(RomanNumeral.arabicToRoman(10), "X");
    }

    @Test
    public void number_is_11_then_XI() {
        assertEquals(RomanNumeral.arabicToRoman(11), "XI");
    }

    @Test
    public void number_is_15_then_XV() {
        assertEquals(RomanNumeral.arabicToRoman(15), "XV");
    }

    @Test
    public void number_is_16_then_XVI() {
        assertEquals(RomanNumeral.arabicToRoman(16), "XVI");
    }

    @Test
    public void number_is_50_then_L() {
        assertEquals(RomanNumeral.arabicToRoman(50), "L");
    }

    @Test
    public void number_is_51_then_LI() {
        assertEquals(RomanNumeral.arabicToRoman(51), "LI");
    }

    @Test
    public void number_is_55_then_LV() {
        assertEquals(RomanNumeral.arabicToRoman(55), "LV");
    }

    @Test
    public void number_is_56_then_LVI() {
        assertEquals(RomanNumeral.arabicToRoman(56), "LVI");
    }

    @Test
    public void number_is_60_then_LX() {
        assertEquals(RomanNumeral.arabicToRoman(60), "LX");
    }

    @Test
    public void number_is_70_then_LXX() {
        assertEquals(RomanNumeral.arabicToRoman(70), "LXX");
    }

    @Test
    public void number_is_80_then_LXXX() {
        assertEquals(RomanNumeral.arabicToRoman(80), "LXXX");
    }

    @Test
    public void number_is_81_then_LXXXI() {
        assertEquals(RomanNumeral.arabicToRoman(81), "LXXXI");
    }

    @Test
    public void number_is_85_then_LXXXV() {
        assertEquals(RomanNumeral.arabicToRoman(85), "LXXXV");
    }

    @Test
    public void number_is_86_then_LXXXVI() {
        assertEquals(RomanNumeral.arabicToRoman(86), "LXXXVI");
    }

    @Test
    public void number_is_126_then_CXXVI() {
        assertEquals(RomanNumeral.arabicToRoman(126), "CXXVI");
    }

    @Test
    public void number_is_2507_then_MMDVII() {
        assertEquals(RomanNumeral.arabicToRoman(2507), "MMDVII");
    }

    @Test
    public void number_is_4_then_IV() {
        assertEquals(RomanNumeral.arabicToRoman(4), "IV");
    }

    @Test
    public void number_is_9_then_IX() {
        assertEquals(RomanNumeral.arabicToRoman(9), "IX");
    }

    @Test
    public void number_is_14_then_XIV() {
        assertEquals(RomanNumeral.arabicToRoman(14), "XIV");
    }

    @Test
    public void number_is_24_then_XXIV() {
        assertEquals(RomanNumeral.arabicToRoman(24), "XXIV");
    }

    @Test
    public void number_is_40_then_XL() {
        assertEquals(RomanNumeral.arabicToRoman(40), "XL");
    }

    @Test
    public void number_is_49_then_XLIX() {
        assertEquals(RomanNumeral.arabicToRoman(49), "XLIX");
    }

    @Test
    public void number_is_90_then_XC() {
        assertEquals(RomanNumeral.arabicToRoman(90), "XC");
    }

    @Test
    public void number_is_99_then_XCIX() {
        assertEquals(RomanNumeral.arabicToRoman(99), "XCIX");
    }

    @Test
    public void number_is_400_then_CD() {
        assertEquals(RomanNumeral.arabicToRoman(400), "CD");
    }

    @Test
    public void number_is_900_then_CM() {
        assertEquals(RomanNumeral.arabicToRoman(900), "CM");
    }

}