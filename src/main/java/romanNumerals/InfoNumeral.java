package romanNumerals;

public class InfoNumeral {
    private final int number;
    private final String info;

    public InfoNumeral(int number, String info) {
        this.number = number;
        this.info = info;
    }

    public int getNumber() {
        return number;
    }

    public String getInfo() {
        return info;
    }
}