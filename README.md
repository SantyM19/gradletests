# GRADLE TESTS #

These are some examples, about how you should implemented Tests with Gradle.

### SETUP ###
Environment Prepared

##### COMMIT:First Commit #####
In this commit, We build a Set Up in Gradle

### SIMPLE TEST ###
How are the tests?

##### COMMIT:Simple Test #####
Simple test Created.

##### COMMIT:Throw an Exception #####
Simple Test Created with Throw an Exception Sentences

### UNIT TEST ###
Implementing Test Units with Junit5

##### COMMIT:Junit5 Unit Test #####
Junit implemented and Test Updating with this Tool

##### COMMIT:Junit Test Organization #####
Test Code Optimized

##### COMMIT: Junit Password Test #####
Password Test Created

### Using MOCKITO ###
Mockito Dependency Added, and some examples Made

##### COMMIT: Mockito Dependency Added #####
Test Payment Service Implemented

##### COMMIT: Test Dice Roll Implemented #####
Applied Mockito this Test

##### COMMIT: Test Code Optimizing Applied #####
Test Payment Service Optimized

### Other Examples ###
Some Examples

- COMMIT: Is Empty Class Implemented
- COMMIT: Leap Year Test Implemented
- COMMIT: Discount Test Applied
- COMMIT: FizzBuzz Test Applied
- COMMIT: Roman Numerals Applied
